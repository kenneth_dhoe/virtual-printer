﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.Windows.Forms

Module Module1
    Public print_data As String

    Sub Main()

        Dim form1 As Form = New Form1

        Dim server As System.Net.Sockets.TcpListener
        Dim host As String = System.Net.Dns.GetHostName()
        Dim LocalHostaddress As String = System.Net.Dns.GetHostByName(host).AddressList(1).ToString()
        Console.WriteLine("Listening on:" & LocalHostaddress & ":9100")
        server = Nothing
        Try
            Dim port As Int32 = 9100
            Dim localaddr As IPAddress = IPAddress.Parse(LocalHostaddress)
            server = New TcpListener(localaddr, port)
            server.Start()
            Dim bytes(1024) As Byte
            Dim data As String = Nothing

            While True
                Console.WriteLine("Waiting for a Print Job...")
                Dim client As TcpClient = server.AcceptTcpClient()
                Console.WriteLine("Print Job Received")
                Console.Beep()
                data = Nothing
                Dim stream As NetworkStream = client.GetStream()

                Dim i As Int32

                ' Loop to receive all the data sent by the client.
                i = stream.Read(bytes, 0, bytes.Length)

                While (i <> 0)

                    ' Translate data bytes to a string.
                    For Each b As Byte In bytes
                        If b = 32 Then
                            b = 0
                        End If
                    Next
                    data = System.Text.Encoding.Default.GetString(bytes, 0, i)

                    data = data.Replace(" ", "")
                    data = data.Replace(vbCr, "").Replace(vbLf, "").Replace(vbNull, "")
                    data = data.Trim

                    Console.Write(data & vbNewLine)

                    i = stream.Read(bytes, 0, bytes.Length)

                    For Each b As Byte In bytes
                        Console.Write(b)
                    Next

                End While
                client.Close()
                Console.WriteLine(vbNewLine & "Print job Completed")
            End While

        Catch e As SocketException
            Console.WriteLine("SocketException: {0}", e)
        End Try
        Console.WriteLine(ControlChars.Cr + "Hit enter to continue....")
        Console.Read()

    End Sub

End Module
